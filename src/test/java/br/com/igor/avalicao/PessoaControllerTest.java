package br.com.igor.avalicao;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.igor.avaliacao.config.ConfigurationLoader;
import br.com.igor.avaliacao.json.PersonAddressJson;
import br.com.igor.avaliacao.json.PersonDebtsJson;
import br.com.igor.avaliacao.json.PersonJson;
import br.com.igor.avaliacao.service.PersonService;

@SpringBootTest(classes = ConfigurationLoader.class)
@AutoConfigureMockMvc
public class PessoaControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void buscarPessoaPorCPF() throws Exception {

		PersonService personService = Mockito.mock(PersonService.class);
		
		PersonJson personJson = mockPersonJson();
		Mockito.when(personService.getByDocument(personJson.getDocument())).thenReturn(personJson);

		mockMvc.perform(get("/person/document/" + personJson.getDocument()).contentType("application/json")
				.content(objectMapper.writeValueAsString(""))).andExpect(status().isOk());

		PersonJson personJsonReturn = personService.getByDocument(personJson.getDocument());

		Assertions.assertThat(personJsonReturn.getDocument().equals(personJson.getDocument()));
		Assertions.assertThat(personJsonReturn.getName().equals(personJson.getName()));
		Assertions.assertThat(personJsonReturn.getAddress().equals(personJson.getAddress()));
		Assertions.assertThat(personJsonReturn.getDebts().equals(personJson.getDebts()));
	}
	
	
	@Test
	void buscarPessoaPorNome() throws Exception {

		PersonService personService = Mockito.mock(PersonService.class);
		PersonJson personJson = mockPersonJson();		

		Mockito.when(personService.getByName(personJson.getName())).thenReturn(personJson);
		
		mockMvc.perform(get("/person/document/" + personJson.getDocument()).contentType("application/json")
				.content(objectMapper.writeValueAsString(""))).andExpect(status().isOk());

		PersonJson personJsonReturn = personService.getByName(personJson.getName());

		Assertions.assertThat(personJsonReturn.getDocument().equals(personJson.getDocument()));
		Assertions.assertThat(personJsonReturn.getName().equals(personJson.getName()));
		Assertions.assertThat(personJsonReturn.getAddress().equals(personJson.getAddress()));
		Assertions.assertThat(personJsonReturn.getDebts().equals(personJson.getDebts()));
	}
	
	PersonJson mockPersonJson() throws Exception {				
				
		PersonJson personJson = new PersonJson();		
		personJson.setName("Igor Camacho");
		personJson.setDocument(BigInteger.valueOf(23051363820L));
		personJson.setAddress(mockPersonAddressJson());
		personJson.setDebts(mockPersonDebtsJson());
		return personJson;				
				
	}
	
	List<PersonAddressJson> mockPersonAddressJson() throws Exception {				
		
		PersonAddressJson personAddressJson = new PersonAddressJson();		
		personAddressJson.setAddress("Av. Nova Cantareira");
		personAddressJson.setNumber("1484");
		personAddressJson.setComplemention("Apto. 71");
		personAddressJson.setDistrict("Tucuruvi");
		personAddressJson.setCity("São Paulo");
		personAddressJson.setState("SP");
		personAddressJson.setCountry("Brasil");
		personAddressJson.setCep("02330-001");
		
		List<PersonAddressJson> personAddressJsonList = new ArrayList<PersonAddressJson>();
				
		personAddressJsonList.add(personAddressJson);
		
		return personAddressJsonList;				
				
	}
	
	List<PersonDebtsJson> mockPersonDebtsJson() throws Exception {				
		
		PersonDebtsJson personDebtsJson = new PersonDebtsJson();		
		personDebtsJson.setDescription("Financiamento Automóvel");
		personDebtsJson.setValue(BigDecimal.valueOf(30000L));
		
		List<PersonDebtsJson> personDebtsJsonList = new ArrayList<PersonDebtsJson>();
		
		personDebtsJsonList.add(personDebtsJson);
		
		return personDebtsJsonList;		
				
	}
}
