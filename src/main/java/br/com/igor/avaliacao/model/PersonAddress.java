package br.com.igor.avaliacao.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode
@NoArgsConstructor
public class PersonAddress {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idAddress;
	
	@NotNull @NotEmpty
	private String address;
	
	@NotNull @NotEmpty
	private String number;
	
	private String complemention;
	
	@NotNull @NotEmpty
	private String district;
	
	@NotNull @NotEmpty
	private String city;
	
	@NotNull @NotEmpty
	private String state;
	
	@NotNull @NotEmpty
	private String country;
	
	@NotNull @NotEmpty
	private String cep;
	
	@ManyToOne
	@JoinColumn(name="id_person")	
	private Person person;
}
