package br.com.igor.avaliacao.model;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode
@NoArgsConstructor
public class Person {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPerson;
	
	@NotNull @NotEmpty
	private String name;
	
	@NotNull @NotEmpty
	private BigInteger document;
	
	@OneToMany(mappedBy="person")
	private List<PersonAddress> address;
	
	@OneToMany(mappedBy="person")
	private List<PersonDebts> debts;
	
}
