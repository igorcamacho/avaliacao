package br.com.igor.avaliacao.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode
@NoArgsConstructor
public class PersonDebts {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDebts;
	
	@NotNull @NotEmpty
	private String description;
	
	@NotNull @NotEmpty
	private BigDecimal value;
	
	@ManyToOne
	@JoinColumn(name="id_person")	
	private Person person;
	
}
