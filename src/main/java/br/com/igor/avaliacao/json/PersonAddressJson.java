package br.com.igor.avaliacao.json;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class PersonAddressJson {

	private int idAddress;
	
	private String address;
	
	private String number;
	
	private String complemention;
	
	private String district;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String cep;

}
