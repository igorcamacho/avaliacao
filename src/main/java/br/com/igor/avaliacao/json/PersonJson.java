package br.com.igor.avaliacao.json;

import java.math.BigInteger;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class PersonJson {

	private int idPerson;
		
	private String name;
	
	private BigInteger document;
		
	private List<PersonAddressJson> address;
	
	private List<PersonDebtsJson> debts;
}
