package br.com.igor.avaliacao.json;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class PersonDebtsJson {

	private int idDebts;
	
	private String description;
	
	private BigDecimal value;
	
}
