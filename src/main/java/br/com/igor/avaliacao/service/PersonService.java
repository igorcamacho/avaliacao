package br.com.igor.avaliacao.service;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.igor.avaliacao.json.PersonAddressJson;
import br.com.igor.avaliacao.json.PersonDebtsJson;
import br.com.igor.avaliacao.json.PersonJson;
import br.com.igor.avaliacao.model.Person;
import br.com.igor.avaliacao.model.PersonAddress;
import br.com.igor.avaliacao.model.PersonDebts;
import br.com.igor.avaliacao.repository.PersonRepository;

@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository;
	
	public PersonJson getByName(String name) throws Exception {	
		
		if (name == null) {
			throw new Exception("Informe o nome para a busca!");
		} 
		
		PersonJson person = convertPersonJson(personRepository.findByName(name));
		return person != null ? person : null;
	}
	
	public PersonJson getByDocument(BigInteger document) throws Exception {	
		
		if (document == null) {
			throw new Exception("Informe o CPF para a busca!");
		} 
		
		return convertPersonJson(personRepository.findByDocument(document));		
	}
	
	private PersonJson convertPersonJson(Person person) {
		PersonJson personJson = null;

		if (person != null) {

			personJson = new PersonJson();
			personJson.setIdPerson(person.getIdPerson());
			personJson.setName(person.getName());
			personJson.setDocument(person.getDocument());

			if (person.getAddress() != null) {
				personJson.setAddress(this.convertPersonAddressJson(person.getAddress()));
			}

			if (person.getDebts() != null) {
				personJson.setDebts(this.convertPersonDebtsJson(person.getDebts()));
			}
		}
		return personJson;
	}
	
	private List<PersonAddressJson> convertPersonAddressJson(List<PersonAddress> personAddress) {
		return personAddress.stream().map(address -> {
			PersonAddressJson personAddressJson = new PersonAddressJson();
			personAddressJson.setIdAddress(address.getIdAddress());
			personAddressJson.setAddress(address.getAddress());
			personAddressJson.setNumber(address.getNumber());
			personAddressJson.setComplemention(address.getComplemention());
			personAddressJson.setDistrict(address.getDistrict());
			personAddressJson.setCity(address.getCity());
			personAddressJson.setState(address.getState());
			personAddressJson.setCountry(address.getCountry());
			personAddressJson.setCep(address.getCep());
			return personAddressJson;
		}).collect(Collectors.toList());
	}
	
	private List<PersonDebtsJson> convertPersonDebtsJson(List<PersonDebts> personDebts) {
		return personDebts.stream().map(debts -> {
			PersonDebtsJson personDebtsJson = new PersonDebtsJson();
			personDebtsJson.setIdDebts(debts.getIdDebts());
			personDebtsJson.setDescription(debts.getDescription());
			personDebtsJson.setValue(debts.getValue());
			return personDebtsJson;
		}).collect(Collectors.toList());
	}
	
}
