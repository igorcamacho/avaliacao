package br.com.igor.avaliacao.controller;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.igor.avaliacao.json.PersonJson;
import br.com.igor.avaliacao.service.PersonService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Controller
@RequestMapping("/person")
public class PersonController {

	private final String mensagemSuccesso = "Busca realizada com sucesso.";
	private final String mensagemNaoEncontrado = "Pessoa não encontrada.";
	private final String mensagemErro = "Erro durante a busca: ";
	
	@Autowired
	PersonService personService;

	@GetMapping("/name/{name}")
	@ApiOperation(value = "Buscar pessoa pelo Nome.")
	@ApiResponses({ @ApiResponse(code = 200, message = mensagemSuccesso), @ApiResponse(code = 204, message = mensagemNaoEncontrado), })
	public ResponseEntity<PersonJson> getPersonByName(@PathVariable("name") String name) {		
		try {
			log.info("Buscando a pessoa pelo nome: " + name);
			PersonJson personJson = personService.getByName(name);
			log.info(mensagemSuccesso + " Nome: " + name);			
			return personJson != null ? ResponseEntity.ok(personJson) : ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error(mensagemErro + e.getMessage());			
			return ResponseEntity.badRequest().build();
		}		

	}
			
	@GetMapping("/document/{document}")
	@ApiOperation(value = "Buscar Pessoa pelo CPF.")
	@ApiResponses({ @ApiResponse(code = 200, message = mensagemSuccesso), @ApiResponse(code = 204, message = mensagemNaoEncontrado), })
	public ResponseEntity<PersonJson> getPersonByDocument(@PathVariable("document") BigInteger document) {		
		try {
			log.info("Buscando a pessoa pelo CPF: " + document);
			PersonJson personJson = personService.getByDocument(document);
			log.info(mensagemSuccesso + " CPF: " + document);			
			return personJson != null ? ResponseEntity.ok(personJson) : ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error(mensagemErro + e.getMessage());			
			return ResponseEntity.badRequest().build();
		}
	}

}
