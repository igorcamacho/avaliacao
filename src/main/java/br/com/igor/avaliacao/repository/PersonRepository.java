package br.com.igor.avaliacao.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.igor.avaliacao.model.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {

	public Person findByName(String nome);
	public Person findByDocument(BigInteger document);
	
}
