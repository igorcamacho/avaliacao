package br.com.igor.avaliacao.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("br.com.igor.avaliacao")
public class ConfigurationLoader {

}
