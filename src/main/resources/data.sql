
insert into Person(document, name) values (23051363820,'Igor Camacho');
insert into Person(document, name) values (99999999999,'Teste Avaliação');

insert into Person_Address(address, number, complemention, district, city, state, country, cep, id_person) 
	values ('Av. Nova Cantareira', 1484, 'Apto 71', 'Tucuruvi', 'São Paulo', 'SP', 'Brasil','02330-001',1);
insert into Person_Address(address, number, complemention, district, city, state, country, cep, id_person)  
	values ('Av. Teste', 123, 'Casa 4', 'Teste', 'São Paulo', 'SP', 'Brasil','99999-999',2);
	
insert into Person_Debts(description, value, id_person)  
	values ('Financiamento Automóvel', 30000, 1);	
	
insert into Person_Debts(description, value, id_person)  
	values ('Teste', 10000, 2);		
