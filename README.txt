A solução proposta para o desafio é apresentada através do arquivo Desafio.pptx na raiz do projeto.

Além disso, foi realizado o desenvolvimento de uma API de exemplo para a consulta a Base A - Serviço 1

Código desenvolvido em Java, versão 1.8, com Spring Boot e uma base de dados sendo simulada com o H2.

Passo a passo para execução do projeto:

1- baixar o projeto através do comando
git clone

2- navegar até o diretório do projeto
avaliacao

3- realizar o build do projeto (via linha de comando utilizando o Maven, ou através de uma IDE)
mvn clean install -P NoDocker

4- executar o jar avalicao-0.0.1-SNAPSHOT.jar contido no diretório target do projeto 
java -jar avalicao-0.0.1-SNAPSHOT.jar


Também é possível executar a imagem dockerizada do projeto, conforme segue:

1- realizar o build do projeto (via linha de comando utilizando o Maven) para a criação da imagem
mvn package

2- executar o comando para execução do container criado
docker run -p 8080:8080 igorcamacho/avaliacao:0.0.1-SNAPSHOT


Após os passos descritos acima, abrir o browser e digitar o endereço: http://localhost:8080/swagger-ui.html
Haverão 2 endpoints de consulta de dados de Pessoa
O primeiro endpoint consulta pelo número do CPF (para efeito de teste utilize o meu CPF 230.513.638-20)
O segundo endpoint consulta pelo nome (para efeito de teste utilize o meu nome Igor Camacho)